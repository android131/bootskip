package com.bootskip.todo.create;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.bootskip.R;
import com.bootskip.databinding.TodoItemCreateFragmentBinding;
import com.bootskip.todo.data.TodoRepository;
import com.bootskip.todo.data.TodoSuggestionsRepository;
import com.bootskip.todo.data.domain.TodoItem;
import com.bootskip.todo.data.domain.TodoList;
import com.bootskip.todo.data.domain.TodoSuggestion;
import com.bootskip.todo.data.domain.TodoUtils;

import java.util.List;

public class TodoItemCreateFragment extends Fragment {

    public static final String TAG = "itemCreateFragment";
    public static final String EXTRA_LIST_ID = "listId";

    private TodoItemCreateFragmentBinding binding;

    private TodoList todoList;
    private List<TodoSuggestion> suggestions;
    private TodoItemSuggestionAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String listId = getArguments().getString(EXTRA_LIST_ID);
        todoList = TodoRepository.getInstance().findTodoList(listId);
        suggestions = TodoSuggestionsRepository.getInstance().getTodoSuggestions();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = TodoItemCreateFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new TodoItemSuggestionAdapter();
        adapter.setItems(suggestions);
        adapter.setOnSuggestionClick(new TodoItemSuggestionAdapter.OnSuggestionClick() {
            @Override
            public void onClick(int index, TodoSuggestion todoSuggestion) {
                addTodoItem(todoSuggestion);
            }
        });
        binding.recyclerView.setAdapter(adapter);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requireActivity().getSupportFragmentManager().popBackStack();
            }
        });
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String itemName = binding.searchView.getQuery().toString();
//                TodoSuggestion todoSuggestion = new TodoSuggestion(itemName);
//                addTodoItem(itemName, 0);
                //TODO add self created Todo item to list
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public void addTodoItem(TodoSuggestion todoSuggestion) {
        TodoItem todoItem = TodoUtils.findTodoItem(todoList, todoSuggestion.name);
        if (todoItem != null){
            todoItem.count = todoItem.count+1;
        } else {
            todoItem = new TodoItem(todoSuggestion.name);
        }
        todoList.items.add(todoItem);
        TodoRepository.getInstance().updateTodoList(todoList);
    }

    public static TodoItemCreateFragment create(TodoList todoList) {
        TodoItemCreateFragment todoItemCreateFragment = new TodoItemCreateFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_LIST_ID, todoList.id);
        todoItemCreateFragment.setArguments(bundle);
        return todoItemCreateFragment;
    }
}
