package com.bootskip.todo.create;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bootskip.R;
import com.bootskip.databinding.TodoListCreateFragmentBinding;
import com.bootskip.todo.data.TodoRepository;
import com.bootskip.todo.data.domain.TodoList;

public class TodoListCreateFragment extends Fragment {

    public static final String TAG = "listCreateFragment";

    private TodoListCreateFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = TodoListCreateFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });

        binding.btCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTodoList();
            }
        });
    }

    public void createTodoList(){
        String listName = binding.nameInput.getText().toString();
        if (listName.isEmpty()) {
            Context context = getContext();
            Toast toast = Toast.makeText(context, R.string.toast_empty_name, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            TodoList todoList = new TodoList(listName);
            TodoRepository.getInstance().saveList(todoList);
            getParentFragmentManager().popBackStack();
        }
    }

    public static TodoListCreateFragment create() { return new TodoListCreateFragment();
    }

}
