package com.bootskip.todo.create;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bootskip.R;
import com.bootskip.todo.data.domain.TodoSuggestion;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class TodoItemSuggestionAdapter extends RecyclerView.Adapter<TodoItemSuggestionAdapter.ViewHolder> {

    private List<TodoSuggestion> list = new ArrayList<>();

    @Nullable
    private OnSuggestionClick onSuggestionClick;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_item_suggestion, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final TodoSuggestion todoSuggestion = list.get(position);
        holder.tvName.setText(todoSuggestion.name);
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (onSuggestionClick != null){
                   onSuggestionClick.onClick(position, todoSuggestion);
               }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnSuggestionClick(OnSuggestionClick listener){
        onSuggestionClick = listener;
    }

    public void setItems(List<TodoSuggestion> todoSuggestions){
        list = todoSuggestions;
    }

    public interface OnSuggestionClick{
         void onClick(int index, TodoSuggestion todoSuggestion);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivAdd;
        TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAdd = itemView.findViewById(R.id.image);
            tvName = itemView.findViewById(R.id.itemName);
        }
    }
}
