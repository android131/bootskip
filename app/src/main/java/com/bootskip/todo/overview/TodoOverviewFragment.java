package com.bootskip.todo.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bootskip.R;
import com.bootskip.databinding.TodoOverviewFragmentBinding;
import com.bootskip.todo.create.TodoListCreateFragment;
import com.bootskip.todo.data.domain.TodoList;
import com.bootskip.todo.details.TodoDetailsFragment;
import com.bootskip.todo.data.TodoRepository;

public class TodoOverviewFragment extends Fragment {

    public static final String TAG = "listOverviewFragment";

    private TodoOverviewFragmentBinding binding;
    private TodoOverviewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = TodoOverviewFragmentBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TodoRepository todoRepository = TodoRepository.getInstance();
        adapter = new TodoOverviewAdapter();
        adapter.setOnListClickListener(new TodoOverviewAdapter.OnListItemClicked() {
            @Override
            public void onClick(TodoList todoList) {
                openListDetails(todoList);
            }
        });
        binding.recyclerView.setAdapter(adapter);
        adapter.setItems(todoRepository.getTodoLists());
        adapter.notifyDataSetChanged();
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCreateList();
            }
        });
    }

    private void openCreateList() {
        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, TodoListCreateFragment.create(), TodoListCreateFragment.TAG)
                .addToBackStack(TodoListCreateFragment.TAG)
                .commit();
    }

    private void openListDetails(TodoList todoList) {
        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, TodoDetailsFragment.create(todoList), TodoDetailsFragment.TAG)
                .addToBackStack(TodoDetailsFragment.TAG)
                .commit();
    }
}
