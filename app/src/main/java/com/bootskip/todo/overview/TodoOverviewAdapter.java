package com.bootskip.todo.overview;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bootskip.R;
import com.bootskip.todo.data.domain.TodoItem;
import com.bootskip.todo.data.domain.TodoList;

import java.util.ArrayList;
import java.util.List;

public class TodoOverviewAdapter extends RecyclerView.Adapter<TodoOverviewAdapter.ViewHolder> {

    private List<TodoList> items = new ArrayList<>();
    private OnListItemClicked clickListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_overview_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TodoList todoList = items.get(position);
        holder.bind(todoList);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<TodoList> items) {
        this.items = items;
    }

    public void setOnListClickListener(OnListItemClicked onListClickListener){
        clickListener = onListClickListener;
    }

    public interface OnListItemClicked{
        public void onClick(TodoList todoList);
    }

     public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView amount;

         ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            amount = itemView.findViewById(R.id.amount);
        }

        void bind(final TodoList todoList){
             title.setText(todoList.name);
             int totalItems = todoList.items.size();
             int checkedItems = 0;
            for (TodoItem item : todoList.items) {
                if (item.isChecked){
                    checkedItems++;
                }
            }
            String s = itemView.getResources().getString(R.string.list_overview_list_item_amount, checkedItems, totalItems);
             amount.setText(s);
             itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (clickListener != null){
                         clickListener.onClick(todoList);
                     }
                 }
             });
        }
    }
}
