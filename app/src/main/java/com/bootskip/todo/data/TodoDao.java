package com.bootskip.todo.data;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.bootskip.todo.data.entity.TodoListWithTodoItems;

import java.util.List;

@Dao
public interface TodoDao {

    @Transaction
    @Query("SELECT * FROM TodoListEntity")
    public List<TodoListWithTodoItems> getListWithItems();
}
