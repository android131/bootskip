package com.bootskip.todo.data;

import androidx.annotation.Nullable;

import com.bootskip.todo.data.domain.TodoList;
import com.bootskip.todo.data.domain.TodoSuggestion;

import java.util.ArrayList;
import java.util.List;

public class TodoSuggestionsRepository {

    private static TodoSuggestionsRepository INSTANCE;
    private List<TodoSuggestion> list = new ArrayList<>();

    private TodoSuggestionsRepository() {
        addSuggestion("Melk");
        addSuggestion("Brood");
        addSuggestion("Eieren");
        addSuggestion("Boter");
        addSuggestion("Kaas");
        addSuggestion("Kip");
        addSuggestion("Toiletpapier");
        addSuggestion("Aardappelen");
        addSuggestion("Sla");
        addSuggestion("Spa");
        addSuggestion("Koffie");
        addSuggestion("Limonade");
        addSuggestion("Chips");
        addSuggestion("Chocolade");
        addSuggestion("Koek");
        addSuggestion("Tandpasta");
        addSuggestion("Shampoo");
        addSuggestion("Ketchup");
        addSuggestion("Mayonaise");
        addSuggestion("Rijst");
    }

    private void addSuggestion(String suggestion){
        list.add(new TodoSuggestion(suggestion));
    }

    public List<TodoSuggestion> getTodoSuggestions() {
        return list;
    }

    public static TodoSuggestionsRepository getInstance(){
        if (INSTANCE == null){
            INSTANCE = new TodoSuggestionsRepository();
        }
        return INSTANCE;
    }
}
