package com.bootskip.todo.data;

import androidx.annotation.Nullable;

import com.bootskip.todo.data.domain.TodoItem;
import com.bootskip.todo.data.domain.TodoList;

import java.util.ArrayList;
import java.util.List;

public class TodoRepository {

    private static TodoRepository INSTANCE;
    private List<TodoList> lists = new ArrayList<>();

    private TodoRepository() {
        TodoList todoList = new TodoList("Test");
        TodoItem todoItem = new TodoItem("Melk");
        todoList.items.add(todoItem);
        lists.add(todoList);
    }

    public List<TodoList> getTodoLists() {
        return lists;
    }

    @Nullable
    public TodoList findTodoList(String todoListId) {
        for (TodoList list : lists) {
            if (todoListId.equals(list.id)){
                return list;
            }
        }
        return null;
    }

    public void updateTodoList(TodoList todoList){
        int i = lists.indexOf(todoList);
        lists.set(i, todoList);
    }

    public void saveList(TodoList todoList){
        lists.add(todoList);
    }

    public static TodoRepository getInstance(){
        if (INSTANCE == null){
            INSTANCE = new TodoRepository();
        }
        return INSTANCE;
    }
}
