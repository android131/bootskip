package com.bootskip.todo.data.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class TodoListWithTodoItems {

    @Embedded
    public TodoListEntity list;

    @Relation(parentColumn = "listId", entityColumn = "listItemId")
    public List<TodoItemEntity> listItems;
}
