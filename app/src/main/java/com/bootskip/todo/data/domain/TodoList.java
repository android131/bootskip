package com.bootskip.todo.data.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class TodoList {

    public String id;
    public String name;
    public List<TodoItem> items;

    public TodoList() {
    }

    public TodoList(String name){
        this.name = name;
        this.id = UUID.randomUUID().toString();
        this.items = new ArrayList<>();
    }

    public TodoList(String id, String name, List<TodoItem> items) {
        this.id = id;
        this.name = name;
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoList todoList = (TodoList) o;
        return id.equals(todoList.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
