package com.bootskip.todo.data.domain;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class TodoItem {

    public String id;
    public String name;
    public int count = 1;
    public boolean isChecked;

    public TodoItem() {
    }

    public TodoItem (String name){
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.isChecked = false;
    }

    public TodoItem(String id, String name, boolean isChecked) {
        this.id = id;
        this.name = name;
        this.isChecked = isChecked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoItem todoItem = (TodoItem) o;
        return id.equals(todoItem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
