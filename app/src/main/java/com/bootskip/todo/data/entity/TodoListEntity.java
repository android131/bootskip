package com.bootskip.todo.data.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TodoListEntity {

    @PrimaryKey
    @NonNull
    public String listId;
    public String name;

}
