package com.bootskip.todo.data.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TodoItemEntity {

    @PrimaryKey
    public String listItemId;
    public String listId;
    public String name;
    public boolean isChecked;


}
