package com.bootskip.todo.data.domain;

public class TodoSuggestion {

    public String name;
    public boolean isAdded;
    public int count;

    public TodoSuggestion(String name){
        this.name = name;
    }

}
