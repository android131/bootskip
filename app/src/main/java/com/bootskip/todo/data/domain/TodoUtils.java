package com.bootskip.todo.data.domain;

import java.util.Iterator;

public class TodoUtils {

    public static TodoItem findTodoItem(TodoList todoList, String todoItemName){
        for (TodoItem item : todoList.items) {
            if (item.name.equalsIgnoreCase(todoItemName)) {
                return item;
            }
        }
        return null;
    }
}
