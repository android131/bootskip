package com.bootskip.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bootskip.R;

public class TodoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_activity);
    }
}
