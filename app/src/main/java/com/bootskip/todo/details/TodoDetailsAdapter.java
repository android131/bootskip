package com.bootskip.todo.details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bootskip.R;
import com.bootskip.todo.data.domain.TodoItem;
import com.bootskip.todo.overview.TodoOverviewAdapter;

import java.util.ArrayList;
import java.util.List;

public class TodoDetailsAdapter extends RecyclerView.Adapter<TodoDetailsAdapter.ViewHolder> {

    private List<TodoItem> todoItems = new ArrayList<>();

    private OnItemClickedListener onItemClickedListener;

    public void setItems(List<TodoItem> items){
        todoItems = items;
    }

    public void setOnItemClickListener(OnItemClickedListener onItemClickListener){
        onItemClickedListener = onItemClickListener;
    }

    @NonNull
    @Override
    public TodoDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_details_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoDetailsAdapter.ViewHolder holder, int position) {
        TodoItem todoItem = todoItems.get(position);
        holder.bind(todoItem);
    }

    @Override
    public int getItemCount() {
        return todoItems.size();
    }

    public interface OnItemClickedListener{
        public void onClick(TodoItem todoItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox checkBox;
        private TextView itemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkbox);
            itemName = itemView.findViewById(R.id.itemName);
        }

        void bind(final TodoItem todoItem){
            itemName.setText(todoItem.name);
            checkBox.setChecked(todoItem.isChecked);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    todoItem.isChecked = isChecked;
                    if (onItemClickedListener != null){
                        onItemClickedListener.onClick(todoItem);
                    }
                }
            });

        }

    }
}
