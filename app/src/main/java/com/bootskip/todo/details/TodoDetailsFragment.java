package com.bootskip.todo.details;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bootskip.R;
import com.bootskip.databinding.TodoDetailsFragmentBinding;
import com.bootskip.todo.create.TodoItemCreateFragment;
import com.bootskip.todo.data.TodoRepository;
import com.bootskip.todo.data.domain.TodoList;

public class TodoDetailsFragment extends Fragment {

    public static final String TAG = "listDetailFragment";
    public static final String EXTRA_ID = "id";

    private String todoListId;
    private TodoDetailsAdapter adapter;
    private TodoDetailsFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = TodoDetailsFragmentBinding.inflate(inflater, container, false);
        todoListId = getArguments().getString(EXTRA_ID);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TodoRepository todoRepository = TodoRepository.getInstance();
        final TodoList todoList = todoRepository.findTodoList(todoListId);
        adapter = new TodoDetailsAdapter();
        adapter.setItems(todoList.items);
        adapter.notifyDataSetChanged();
        binding.recyclerView.setAdapter(adapter);
        binding.toolbar.setTitle(todoList.name);

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requireActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, TodoItemCreateFragment.create(todoList) ,TodoItemCreateFragment.TAG)
                        .addToBackStack(TodoDetailsFragment.TAG)
                        .commit();
            }
        });
    }

    public static TodoDetailsFragment create(TodoList todoList){
        TodoDetailsFragment fragment = new TodoDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_ID, todoList.id);
        fragment.setArguments(bundle);
        return fragment;
    }

}
