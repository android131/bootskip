package com.bootskip.core;

import androidx.room.RoomDatabase;

import com.bootskip.todo.data.entity.TodoListEntity;

@androidx.room.Database(entities = {TodoListEntity.class}, version = 1)
abstract class Database extends RoomDatabase {
}
